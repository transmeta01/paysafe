package com.example.paysafe.accountmonitor;

import com.example.paysafe.domain.MonitoringStatus;
import com.example.paysafe.domain.Report;
import com.example.paysafe.utils.MonitoringTask;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

/**
 * Copyright 2018/02 www.cogalab.com
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @author <a href="richard.mutezintare@cogalab.com">Richard Mutezintare</a>
 *
 * The app should have REST endpoint(s) that allows to start or stop the monitoring of the Paysafe Developer API.
 * In its payload, this endpoint(s) should accept at least these two values:
 * An interval field that tells your server what interval should be used between each request to the server(start).
 * A url field for the url value of the server to monitor(start and stop).
 *
 * <p>
 * The app should have another REST endpoint which, when requested, gives an overview to the user of the time periods
 * when the server was up and when it was down since the last time it was started.
 * <p>
 *
 */

@RestController
@RequestMapping(value = "v1")
public class MonitorController {
    Logger logger = LoggerFactory.getLogger( this.getClass());

    Date startTimestamp;
    Date stopTimestamp;

    MonitoringTask task;

    private ScheduledExecutorService scheduler;
    private ScheduledFuture<?> currentFuture;

    private Report report;

    private final String PAYSAFE_ENDPOINT = "https://private-anon-a7e496b23d-paysafeapiaccountmanagementv1.apiary-mock.com/accountmanagement/monitor";

    public MonitorController() {
        this.report = new Report();
    }

    /**
     * @param interval in seconds
     * @return
     */
    @RequestMapping(value = "/start_monitoring", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity startMonitoring(@RequestParam("interval") int interval) {
        if(report == null) report = new Report();

        if(scheduler == null) {

            scheduler = Executors.newScheduledThreadPool( 2 );

            logger.info( "============= START MONITORING REQUESTED ==============" );

            startTimestamp = new Date();
            report.getReport().put( startTimestamp.getTime() , "start event" );


            MonitoringStatus monitoringStatus = new MonitoringStatus();
            task = new MonitoringTask( PAYSAFE_ENDPOINT , monitoringStatus );
            currentFuture = scheduler.scheduleAtFixedRate( task , 0 , interval , TimeUnit.SECONDS );
        }

        return ResponseEntity.ok().build();
    }

    @RequestMapping(value = "/stop_monitoring", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity stopMonitoring() {
        if(stopTimestamp == null) stopTimestamp = new Date();
        report.getReport().put(stopTimestamp.getTime(), "stop event");

        logger.info("============= STOP MONITORING REQUESTED ==============");

        if(scheduler != null) {
            currentFuture.cancel(true);
            scheduler.shutdownNow();
            scheduler = null;
        }

        return ResponseEntity.accepted().build();
    }

    @RequestMapping(value = "/report_monitoring", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Report> report() {
        report.setMessage(computeTimeElapseBetweenStartAndStop(startTimestamp, stopTimestamp));

        return new ResponseEntity(report, HttpStatus.OK);
    }

    private Long computeTimeElapseBetweenStartAndStop(Date startTimestamp, Date stopTimestamp) {
        if(startTimestamp == null) return 0L;
        else if(stopTimestamp == null) return startTimestamp.getTime();

        return startTimestamp.compareTo(stopTimestamp) < 0 ?
                stopTimestamp.getTime() - startTimestamp.getTime():
                startTimestamp.getTime() - stopTimestamp.getTime();
    }

}
