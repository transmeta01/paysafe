package com.example.paysafe.domain;

import lombok.Data;

import java.util.HashMap;
import java.util.Map;

/**
 * Copyright 2018/02 www.cogalab.com
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @author <a href="richard.mutezintare@cogalab.com">Richard Mutezintare</a>
 */

@Data
public class Report {

    private Map<Long, String> report;
    private String message;

    public Report() {
        report = new HashMap<>();
        message = "No Data available yet";
    }

    public Map<Long, String> getReport() {
        return report;
    }

    /**
     * @param interval in milliseconds
     */
    public void setMessage(Long interval) {
        message = String.format("seconds between latest start and stop of server %d seconds", interval / 1000);
    }

    public String getMessage() {
        return this.message;
    }
}
