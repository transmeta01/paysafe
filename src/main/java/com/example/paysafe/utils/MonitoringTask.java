package com.example.paysafe.utils;

import com.example.paysafe.domain.MonitoringStatus;
import com.example.paysafe.domain.Status;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.client.RestTemplate;

import java.util.Date;
import java.util.TimerTask;

/**
 * Copyright 2018/02 www.cogalab.com
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * @author <a href="richard.mutezintare@cogalab.com">Richard Mutezintare</a>
 */

public class MonitoringTask extends TimerTask {

    Logger logger = LoggerFactory.getLogger(this.getClass().getName());

    private String monitoringUrl;
    private MonitoringStatus monitoringStatus;

    public MonitoringTask() {}

    public MonitoringTask(String url, MonitoringStatus monitoringStatus) {
        monitoringUrl = url;
        this.monitoringStatus = monitoringStatus;
    }

    @Override
    public void run() {
        RestTemplate restTemplate = new RestTemplate();

        logger.info(String.format("======== FETCHING %s ========", monitoringUrl));

        Status status = restTemplate.getForObject(monitoringUrl, Status.class);

        logger.info("======== ACCOUNT MONITORING ======== " + status.getStatus());

        this.monitoringStatus = new MonitoringStatus(status.getStatus(), new Date());
    }

    public MonitoringStatus getMonitoringStatus() {
        return monitoringStatus;
    }
}
